#!/usr/bin/env bash
# Использование set для остановки скрипта при возникновении ошибок
set -euo pipefail
# Задание часового пояса
ln -sf /usr/share/zoneinfo/Asia/Omsk /etc/localtime
# Генерация файла /etc/adjtime
hwclock --systohc
# Локализация
sed -i -e 's/#en_US.UTF-8/en_US.UTF-8/; s/#ru_RU.UTF-8/ru_RU.UTF-8/' /etc/locale.gen
locale-gen
echo 'LANG=ru_RU.UTF-8' > /etc/locale.conf
echo -e 'KEYMAP=ru\nFONT=cyr-sun16' > /etc/vconsole.conf
# Настройка сети
echo -n 'Введите имя хоста: '
read HOSTNAME
echo "$HOSTNAME" > /etc/hostname
echo -e "127.0.0.1\tlocalhost\n::1      \tlocalhost\n127.0.1.1\t$HOSTNAME" >> /etc/hosts
# Регенерация initramfs
mkinitcpio -P
# Задание пароля для суперпользователя
echo 'Укажите пароль для суперпользователя (root)'
passwd
# Установка загрузчика
if echo "$DEVICE" | grep -wq "[svh]d[a-z]"
then
    if [[ -d /sys/firmware/efi/efivars ]]
    then
    bootctl --esp-path=/boot install
    echo -e 'default   arch.conf\nconsole-mode max\neditor   no' > /boot/loader/loader.conf
    echo -e "title   Arch Linux\nlinux   /vmlinuz-linux-lts\ninitrd  /intel-ucode.img\ninitrd  /initramfs-linux-lts.img\noptions root=UUID=$(lsblk -dno UUID /dev/$DEVICE\1) rw" > /boot/loader/entries/arch.conf
    echo -e "title   Arch Linux (fallback initramfs)\nlinux   /vmlinuz-linux-lts\ninitrd  /intel-ucode.img\ninitrd  /initramfs-linux-lts-fallback.img\noptions root=UUID=$(lsblk -dno UUID /dev/$DEVICE\1) rw" > /boot/loader/entries/arch-fallback.conf
    else
    grub-install /dev/$DEVICE
    grub-mkconfig -o /boot/grub/grub.cfg
    fi
elif echo "$DEVICE" | grep -Ewq "nvme*|mmcblk*"
then
    if [[ -d /sys/firmware/efi/efivars ]]
    then
    bootctl --esp-path=/boot install
    echo -e 'default   arch.conf\nconsole-mode max\neditor   no' > /boot/loader/loader.conf
    echo -e "title   Arch Linux\nlinux   /vmlinuz-linux-lts\ninitrd  /intel-ucode.img\ninitrd  /initramfs-linux-lts.img\noptions root=UUID=$(lsblk -dno UUID /dev/$DEVICE\p1) rw" > /boot/loader/entries/arch.conf
    echo -e "title   Arch Linux (fallback initramfs)\nlinux   /vmlinuz-linux-lts\ninitrd  /intel-ucode.img\ninitrd  /initramfs-linux-lts-fallback.img\noptions root=UUID=$(lsblk -dno UUID /dev/$DEVICE\p1) rw" > /boot/loader/entries/arch-fallback.conf
    else
    grub-install /dev/$DEVICE
    grub-mkconfig -o /boot/grub/grub.cfg
    fi
fi
