#!/usr/bin/env zsh
# Использование set для остановки скрипта при возникновении ошибок
set -euo pipefail
# Синхронизация системных часов
timedatectl set-ntp true
sleep 90
# Изменение шрифта для отображения кириллицы
setfont cyr-sun16
# Очистка терминала
clear
# Список устройств
fdisk -l
# Выбор устройства для разметки
echo -n 'Выберите устройство для разметки (например, sda, nvme0n1 или mmcblk0): '
read DEVICE
# Создание, форматирование и монтирование разделов
if echo "$DEVICE" | grep -wq "[svh]d[a-z]"
then
    if [[ -d /sys/firmware/efi/efivars ]]
    then
    echo -n 'Укажите объем корневого раздела (например, 100G или 1T): '
    read ROOTSIZE
    echo -n 'Укажите объем системного раздела EFI (например, 300M или + для использования всего свободного места): '
    read ESPSIZE
    echo -e ",$ROOTSIZE,L\n,$ESPSIZE,U\n" | sfdisk -X gpt -w always -W always --lock /dev/$DEVICE
    mkfs.ext4 /dev/$DEVICE\1
    mkfs.fat -F 32 /dev/$DEVICE\2
    mount /dev/$DEVICE\1 /mnt
    mount -m /dev/$DEVICE\2 /mnt/boot
    pacstrap /mnt base linux-lts linux-firmware intel-ucode iwd opendoas nano
    else
    echo -n 'Укажите объем корневого раздела (например, 100G или + для использования всего свободного места): '
    read ROOTSIZE
    echo -e ",$ROOTSIZE,L\n" | sfdisk -X dos -w always -W always --lock /dev/$DEVICE
    mkfs.ext4 /dev/$DEVICE\1
    mount /dev/$DEVICE\1 /mnt
    pacstrap /mnt base linux-lts linux-firmware grub intel-ucode iwd opendoas nano
    fi
elif echo "$DEVICE" | grep -Ewq "nvme<0->n<1->|mmcblk<0->"
then
    if [[ -d /sys/firmware/efi/efivars ]]
    then
    echo -n 'Укажите объем корневого раздела (например, 100G или 1T): '
    read ROOTSIZE
    echo -n 'Укажите объем системного раздела EFI (например, 300M или + для использования всего свободного места): '
    read ESPSIZE
    echo -e ",$ROOTSIZE,L\n,$ESPSIZE,U\n" | sfdisk -X gpt -w always -W always --lock /dev/$DEVICE
    mkfs.ext4 /dev/$DEVICE\p1
    mkfs.fat -F 32 /dev/$DEVICE\p2
    mount /dev/$DEVICE\p1 /mnt
    mount -m /dev/$DEVICE\p2 /mnt/boot
    pacstrap /mnt base linux-lts linux-firmware intel-ucode iwd opendoas nano
    else
    echo -n 'Укажите объем корневого раздела (например, 100G или + для использования всего свободного места): '
    read ROOTSIZE
    echo -e ",$ROOTSIZE,L\n" | sfdisk -X dos -w always -W always --lock /dev/$DEVICE
    mkfs.ext4 /dev/$DEVICE\p1
    mount /dev/$DEVICE\p1 /mnt
    pacstrap /mnt base linux-lts linux-firmware grub intel-ucode iwd opendoas nano
    fi
else
echo 'Извините, но скрипт предназначен только для установки на устройства, имена которых совпадают с /dev/sd*, /dev/hd*, /dev/vd*, /dev/nvme*, /dev/mmcblk*'
exit
fi
# Генерация файла /etc/fstab
genfstab -U /mnt >> /mnt/etc/fstab
# Вход в chroot
curl -o /mnt/root/archbaseP2.sh https://gitlab.com/f1r3m00n/chocolatte/-/raw/main/archbaseP2.sh
sed -i "3a\DEVICE=$DEVICE" /mnt/root/archbaseP2.sh
chmod +x /mnt/root/archbaseP2.sh
arch-chroot /mnt /root/archbaseP2.sh
rm -f /mnt/root/archbaseP2.sh
# Размонтирование разделов
umount -R /mnt
# Выключение системы
shutdown -h +0
